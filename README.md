# costool

[TOC]

一个腾讯云cos（对象存储）非官方快速上传和下载的工具，使用官方go-sdk二次开发。可以用于以下场景。

- 备份一些配置文件，比如`.bashrc` `.vimrc` `.gdbinit`等。
- 快速部署静态网页，设置一键部署指令。
- 多设备传送文件 。
- 其他骚操作。

目前的功能有：

- [x] 上传和下载文件。上传默认覆盖文件。
- [x] 列出文件和选择存储桶。
- [x] 显示上传和下载进度。
- [ ] 删除文件。

使用cos做图床，可以看这个项目，也可以使用Pic-Go之类的。

https://gitee.com/qi_xmu/quplor-go.git

## 使用截图

![img](https://quplor-1300340355.cos.ap-guangzhou.myqcloud.com/QUPLOR/2022-05-07/README/1651926791156231200.png)

## 使用方法

###  配置文件

需要放在用户目录下的`.costool.yaml`文件中格式如下：

```yaml
UserID: "xxxx"
SecretID: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
SecretKey: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Token: ""
```

注意，键值之间存在空格。(建议使用项目密钥，相对安全)

![img](https://quplor-1300340355.cos.ap-guangzhou.myqcloud.com/QUPLOR/2022-05-07/README/1651926753837745700.png) 

## 安装方法

1. 下载预编译包

   [第一个完整版本 · @奇/costool - Gitee.com](https://gitee. com/qi_xmu/costool/releases/v1.0) 

2. 自行编译，需要安装go环境

  ```bash
  git clone https://gitee.com/qi_xmu/costool.git
  cd costool
  make && sudo make install
  ```

## 其他

遇到bug或者问问题，可以提出issuse反馈。
